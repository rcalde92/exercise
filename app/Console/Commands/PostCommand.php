<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use GuzzleHttp\Client;
use PHPUnit\Exception;
use GuzzleHttp\Promise as GuzzlePromise;


class PostCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'request:post';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Make POST to https://atomic.incfile.com/fakepost';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $retryOnError = true;
        try
        {
            $this->post();

        }catch (\GuzzleHttp\Exception\BadResponseException $e)
        {
            $acum=0;
            $success=false;
            while ($acum<10)
            {
                try {
                    $this->post();
                    $acum=10;
                    $success=true;
                }catch (\GuzzleHttp\Exception\BadResponseException $e)
                {
                    $acum++;
                }
            }
            if ($success==false)
            {
                $this->error('An error ocurred during your request');
            }
        }
        try
        {
            $this->multiplePost();

        }catch (\GuzzleHttp\Exception\BadResponseException $e)
        {
            $this->error('An error ocurred during your multiple requests');

        }
    }
    public function multiplePost()
    {
        //Answer to question 5
        $client = new Client([
            'base_uri' => 'https://atomic.incfile.com',
            'timeout'  => 2.0,
        ]);
        $values=[['test1','test2'],['test3','test4']];
        foreach ($values as $value) {

            $request = $client->request('POST', 'https://atomic.incfile.com/fakepost', [
                'form_params' => [
                    'field1' => $value[0],
                    'field2' => $value[1]

                ]
            ]);
            $promise = $client->sendAsync($request)->then(function ($response) {
                $this->line('POST Successful');
            });
            $promise->wait();
        }
        $requestPromises = [];

    }
    public function post()
    {
        //Answer to question 4
        $client = new Client([
            'base_uri' => 'https://atomic.incfile.com',
            'timeout'  => 2.0,
        ]);
        $response = $client->request('POST', 'https://atomic.incfile.com/fakepost', [
            'form_params' => [
                'field1' => 'test',
                'field2' => 'test2'

            ]
        ]);


        $statuscode=$response->getStatusCode();
        if (200 === $statuscode) {
            $this->line('POST Successful');
        }
        elseif (404 === $statuscode) {
            $this->error('Page not found');
        }
        else {
            throw new MyException("Invalid response from API");
        }
    }
}
